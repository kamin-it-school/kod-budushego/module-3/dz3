package com.company;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;


@Data
@AllArgsConstructor
public class Student implements Serializable {
    private String firstName;
    private String lastName;
    private String ticket;
    private Integer age;
}
