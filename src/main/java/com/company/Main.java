package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String ser_path = "ob.ser";
        Scanner scanner = new Scanner(System.in);
        Journal jr = new Journal();
        if(new File(ser_path).exists()) {
            FileInputStream fileInputStream = new FileInputStream(ser_path);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            jr = (Journal) objectInputStream.readObject();
        }
        String menu = """
                Введите номер команды:
                1.Добавить студента
                2.Удалить студента
                3.Поиск студента
                4.Изменение студента
                5.Загрузить состояние журнала
                6.Сохранить состояние журнала
                7.Выход""";
        while(true){
            System.out.println(menu);
            try {
                int number = scanner.nextInt();
                switch (number){
                    case 1 ->{
                        System.out.println("Введите имя:");
                        String name = scanner.next();
                        System.out.println("Введите фамилию:");
                        String lastName = scanner.next();
                        System.out.println("Введите номер студ билета:");
                        String ticket = scanner.next();
                        System.out.println("Введите возраст");
                        Integer age = scanner.nextInt();
                        jr.createStudent(name,lastName,ticket,age);
                        System.out.println("Успешно создано.");
                        serialize(jr,ser_path);
                    }
                    case 2 ->{
                        System.out.println("Введите номер студ билета:");
                        String ticket = scanner.next();
                        if(jr.deleteStudent(ticket)){
                            System.out.println("Студент удален");
                            serialize(jr,ser_path);
                        }else{
                            System.out.println("Студент не найден");
                        }
                    }
                    case 3 ->{
                        System.out.println("""
                                Выберите условия поиска
                                1.Имя
                                2.Фамилия
                                3.Студенческий бидет
                                4.Возраст""");
                        int task = scanner.nextInt();
                        ArrayList<Student> forPrint = null;
                        switch (task) {
                            case 1 ->{
                                System.out.println("Введите имя");
                                String find = scanner.next();
                                forPrint = jr.find(find,null,null,null);
                            }
                            case 2 ->{
                                System.out.println("Введите фамилию");
                                String find = scanner.next();
                                forPrint = jr.find(null,find,null,null);
                            }
                            case 3 ->{
                                System.out.println("Введите номер студ билета");
                                String find = scanner.next();
                                forPrint = jr.find(null,null,find,null);
                            }
                            case 4 ->{
                                System.out.println("Введите возраст");
                                Integer find = scanner.nextInt();
                                forPrint = jr.find(null,null,null,find);
                            }
                        }
                        if(forPrint != null && !forPrint.isEmpty()){
                            for (Student s:forPrint){
                                System.out.println("\n_____");
                                System.out.println(s);
                            }
                            System.out.println("\n");
                        }else{
                            System.out.println("Нет данных");
                        }
                    }
                    case 4 ->{
                        System.out.println("Введите номер студ билета для определения изменяемого студента. Если студент не будет найден - будет создана новая учетная запись:");
                        String ticket = scanner.next();
                        System.out.println("Введите имя:");
                        String name = scanner.next();
                        System.out.println("Введите фамилию:");
                        String lastName = scanner.next();
                        System.out.println("Введите возраст");
                        Integer age = scanner.nextInt();
                        jr.changeStudent(name,lastName,ticket,age);
                        System.out.println("Успешно обновлено.");
                        serialize(jr,ser_path);
                    }
                    case 5 ->{
                        if(new File(ser_path).exists()) {
                            FileInputStream fileInputStream = new FileInputStream(ser_path);
                            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                            jr = (Journal) objectInputStream.readObject();
                        }else{
                            System.out.println("Сохраненных данных нет.");
                        }
                    }
                    case 6 ->{
                        serialize(jr,ser_path);
                    }
                    case 7 ->{
                        serialize(jr,ser_path);
                        System.exit(0);
                    }
                }
            }catch(Exception e){
                System.out.println("Что-то пошло не так, " +
                        "попробуйте снова" + e.getMessage());
            }
        }
    }
    private static void serialize(Journal journal, String path) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(path);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(journal);
        objectOutputStream.close();
    }
}