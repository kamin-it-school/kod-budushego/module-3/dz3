package com.company;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Journal implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    ArrayList<Student> list;

    public void createStudent(String name, String lastName, String ticket, Integer age){
        list.add(new Student(name,lastName,ticket,age));
    }

    public boolean deleteStudent(String ticket){
        boolean deleted = false;
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getTicket().equals(ticket)){
                list.remove(list.get(i));
                deleted = true;
                break;
            }
        }
        return deleted;
    }

    public void changeStudent(String name, String lastName, String ticket, Integer age){
        deleteStudent(ticket);
        createStudent(name, lastName, ticket, age);
    }

    public ArrayList<Student> find(String name, String lastName, String ticket, Integer age){
        ArrayList<Student> find = new ArrayList<>();
        for (Student student : list) {
            if (student.getTicket().equals(ticket) ||
                    student.getFirstName().equals(name) ||
                    student.getLastName().equals(lastName) ||
                    Objects.equals(student.getAge(), age)) {
                find.add(student);
            }
        }
        return find;
    }

    public Journal() {
        this.list = new ArrayList<>();
    }
}
